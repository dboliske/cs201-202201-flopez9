package exams.first;

import java.util.Scanner;

public class Pet {
	
	private String name = "";
	private int age = 0;
	
	public Pet() {
		
		name = "";
		age = 0;
	}
	
	public Pet(String name, int age) {
		
		setName(name);
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		} else {
			this.age = 0;
		}
	}

	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if (age != p.getAge()) {
			return false;
		} else if (name != p.getName()) {
			return false;
		}
		
		return true;
	}
	
	public String toString() {
		return "Your pet's name is " + name + "and its age is " + age + ".";
	}
	
	public static Pet constructPet() {
		return constructPet(new Scanner(System.in));
	}
	
	private static Pet constructPet(Scanner scanner) {
		return null;
	}
	
	public static Pet constructPet(Scanner input) {
		
		Pet r = new Pet();
		
		r.setName(getString(input, "Enter a name: "));
		r.setAge(getInt(input, "Enter an age: "));
		
		return r;
	}

	private static int getInt(Scanner input, String string) {
		
		return 0;
	}

	private static String getString(Scanner input, String string) {
	
		return null;
	}
}
