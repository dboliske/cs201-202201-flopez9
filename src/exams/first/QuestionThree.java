package exams.first;

import java.util.Scanner;

public class QuestionThree {
	
	public static void main(String[] args) {
			
			Scanner input = new Scanner(System.in);
			
			System.out.println("Enter a number: ");
			int length = input.nextInt();
			
			for (int i = 0; i <= length; i++) {
				
				for (int j = length; j >= i; j--) {
					System.out.print(" ");
				}
				
				for (int j = length; j <= i; j++) {
					System.out.print("* ");
				}
				
				System.out.println();
			}
	}

}
