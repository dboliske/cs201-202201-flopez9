package exams.first;

import java.util.Scanner;

public class QuestionTwo {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Type an integer: ");
		int num = input.nextInt();
		
		if ( num % 2 == 0 && num % 3 == 0 ) {
			System.out.println("foobar");
		} else if (num % 3 == 0) {
			System.out.println("bar");
		} else if (num % 2 == 0) {
			System.out.println("foo");
		}
		
	}
}
