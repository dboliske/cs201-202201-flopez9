# Midterm Exam

## Total

88/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  16/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               1/5
4. Arrays:                      15/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     17/20
    - Variables:                4/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      3/5

## Comments

1. Good
2. Good
3. On the right track, but formatting isn't there.
4. Doesn't print out words that occur more than once.
5. Good start, but has compiler errors preventing use and the `equals` method does not follow the UML diagram and have an Object parameter nor correctly compare the `names`.
