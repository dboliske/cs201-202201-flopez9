package exams.second;

public class Classroom {
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		
		building = "";
		roomNumber = "";
		seats = 0;
	}
	
	public Classroom(String building, String roomNumber, int seats) {
		
		setBuilding(building);
		setRoomNumber(roomNumber);
		setSeats(seats);
	}
	
	public void setBuilding(String building) {
		
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) {
		
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) {
		
		if (seats > 0) {
			this.seats = seats;
		} else if (seats < 0) {
			this.seats = 0;
		}
	}
	
	public String getBuilding() {
		
		return building;
	}
	
	public String getRoomNumber() {
		
		return roomNumber;
	}
	
	public int getSeats() {
		
		return seats;
	}
	
	public String toString() {
		
		return "This classroom is in building " + building + " in room number " + roomNumber + " it has " + seats + " seats available.";
	}

	
}
