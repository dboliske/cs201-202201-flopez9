package exams.second;

public class Circle extends Polygon {
	
	private double radius;
	
	public Circle() {
		
		super();
		radius = 1;
	}
	
	public Circle(String name, double radius) {
		
		super(name);
		setRadius(radius);
	}
	
	public void setRadius(double radius) {
		
		if (radius > 0) {
			this.radius = radius;
		} else if (radius < 0) {
			this.radius = 1;
		}
	}
	
	public double getRadius() {
		
		return radius;
	}
	
	public String toString() {
		
		return "This circle is named " + name + " its has a radius of " + radius;
	}
	
	
	
	public double area() {
		
		return Math.PI * radius * radius;
	}
	
	public double perimiter() {
		
		return 2.0 * Math.PI * radius;
	}

}
