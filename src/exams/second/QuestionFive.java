package exams.second;

import java.util.Scanner;

public class QuestionFive {
	
	public static double JumpSearch(double[] a, double target) {
		
		double m = (int) Math.sqrt(a.length);
		double p = 0;
		
		while (a[(int) Math.min(m, a.length - 1)] < target) {
			p = m;
			m += (int) Math.sqrt(a.length);
			
			if (p >= a.length) {
				return -1;
			}
		}
		
		while (a[(int) p] < target) {
			p++;
			if (p == Math.min(m, a.length - 1)) {
				return -1;
			}
		}
		
		if (a[(int) p] == target) {
			return p;
		}
		
		return -1;
	}
	
	public static void main(String[] args) {
		double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.println("Search number: ");
		double target = input.nextDouble();
		double index = JumpSearch(numbers, target);
		if (index == -1 ) {
			System.out.println("-1");
		} else {
			System.out.println(target + " found at index " + index + ".");
		}
	}

}
