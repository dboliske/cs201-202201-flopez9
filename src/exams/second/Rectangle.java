package exams.second;

public class Rectangle extends Polygon {
	
	private double width;
	private double height;
	
	public Rectangle() {
		
		super();
		width = 1;
		height = 1;
	}
	
	public Rectangle(String name, double width, double height) {
		
		super(name);
		setWidth(width);
		setHeight(height);
	}
	
	public void setWidth(double width) {
		
		if (width > 0) {
			this.width = width;
		} else if (width < 0) {
			this.width = 1;
		}
	}
	
	public void setHeight(double height) {
		
		if (height > 0) {
			this.height = height;
		} else if (height < 0) {
			this.height = 1;
		}
	}
	
	public double getWidth() {
		
		return width;
	}
	
	public double getHeight() {
		
		return height;
	}
	
	public String toString() {
		
		return "This rectangle is named " + name + " its has a width of " + width + " and a height of " + height;
	}
	
	
	
	public double area() {
		
		return height * width;
	}
	
	public double perimiter() {
		
		return 2.0 * height * width;
	}

}

