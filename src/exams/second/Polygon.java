package exams.second;

public class Polygon {
	
	protected String name;
	
	public Polygon() {
		
		name = "";
	}
	
	public Polygon(String name) {
		
		setName(name);
	}
	
	public void setName(String name) {
		
		this.name = name;
	}
	
	public String getName() {
		
		return name;
	}
	
	public String toString() {
		
		return "This shape is called: " + name;
	}
	
	
	
	public double area() {
			
		return area();
	}
	
	public double perimeter() {
		
		return perimeter();
	}

}
