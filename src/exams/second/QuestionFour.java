package exams.second;

public class QuestionFour {
	
	public static void SelectionSort(String[] a) {
		
		int i;
		int j;
		
		for (i = 0;  i < a.length-1; i++) {
			int min = i;
			
			for (j = i + 1; j < a.length; j++) {
				if (a[j].compareTo(a[min]) < 0) {
					min = j;
				}
			}
			
			String temp = a[i];
			a[i] = a[min];
			a[min] = temp;
		}
		
	}
	
	public static void main(String[] args) {
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		SelectionSort(words);
		
		for(String w : words) {
			System.out.println(w);
		}
	}
	
}
	
	
