package exams.second;

public class ComputerLab extends Classroom {
	
	private boolean computers;

	public ComputerLab() {
		
		super();
		computers = true;
	}
	
	public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
		
		super(building, roomNumber, seats);
		setComputers(computers);
	}
	
	public void setComputers(boolean computers) {
		
		if (computers == true) {
			this.computers = computers;
		} else if (computers == false) {
			this.computers = false;
		}
	}
	
	public boolean hasComputers() {
		
		return computers;
	}
	
	public String toString() {
		
		return "If the classroom in building " + building + " and room number " + roomNumber + " is a computer lab with " + getSeats() + " seats available.";
	}
	
	
}
