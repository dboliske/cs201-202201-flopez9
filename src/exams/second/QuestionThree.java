package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThree {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		ArrayList<Integer> numbers =  new ArrayList<Integer>();
		
		for (int i = 0; i < 10;  i++) {
			
			System.out.println("Enter a number (type 'Done' when finished): ");
			numbers.add(Integer.parseInt(input.nextLine()));
					
		}
		
		for (Integer i : numbers) {
			
			System.out.print(i + ", ");
		}
		
		input.close();
	}

}
