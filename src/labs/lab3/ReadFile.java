package labs.lab3;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class ReadFile {

	public static void main(String[] args) {
		
		try {
			File Students = new File("src/labs/lab3/grades.csv");
			Scanner Class = new Scanner(Students);
			
			while (Class.hasNextLine()) {
				String data = Class.nextLine();
				String[] values = data.split(",");
			}
			Class.close();
		} catch (FileNotFoundException e) {
			System.out.println("This file doesn't exist!");
			System.out.println(e.getMessage());
		}
		

	}

}
