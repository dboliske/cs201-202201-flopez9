package labs.lab1;

import java.util.Scanner;

public class Box {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		int H;
		int L;
		int W;
		
		System.out.println("Enter a height ");
		H = input.nextInt();
		
		System.out.println("Enter a length ");
		L = input.nextInt();
		
		System.out.println("Enter a width ");
		W = input.nextInt();
		
		System.out.println("You will need " + (L * H + L * W + W * H) + " square feet of wood to make a box of with these dimensions.");
		//calculates the amount of wood needed for the size of the box
	}

}
