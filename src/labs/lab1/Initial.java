package labs.lab1;

import java.util.Scanner;

public class Initial {

	public static void main(String[] args) {
	
		Scanner input = new Scanner(System.in);
		
		String Name;
		
		System.out.print("What is your first name?");
		Name = input.nextLine();
		
		System.out.print("Your first initial is: " + Name.charAt(0));
		// Outputs the first letter of the name that the user inputed
	}

}
