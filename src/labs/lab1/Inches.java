package labs.lab1;

import java.util.Scanner;

public class Inches {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int Inches;
		int Centimeters;
		
		System.out.print("Give a length in inches: ");
		Inches = input.nextInt();
		
		System.out.println("The length in centimeters is: " + (Inches * 2.54));
		//converts inches to centimeters
		
		System.out.println("\n");
		
		System.out.print("Give a length in centimeters: ");
		Centimeters = input.nextInt();
		
		System.out.println("The length in inches is: " + (Centimeters / 2.54));
		//converts centimeters to inches
	}

}
