package labs.lab1;

import java.util.Scanner;

public class Name {

	public static void main(String[] args) {
		
			System.out.print("What is your name?");
			//asks user their name
			
			Scanner input = new Scanner(System.in);
			String name = input.nextLine();
			// makes the name be what the user inputed
			
			System.out.println("Your name is: " + name);
	}

}
