package labs.lab1;

import java.util.Scanner;

public class Calculations {

	public static void main(String[] args) {

		int YourAge;
		int DadsAge;
		int BirthYear;
		double Height;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("How old are you?");
		YourAge = input.nextInt();
		
		System.out.println("How old is your dad?");
		DadsAge = input.nextInt();
		
		System.out.println("Yours Dads age minus Your age is: " + (DadsAge - YourAge));
		// subtracts user's age from their dad's
		
		System.out.println("\n");
		
		System.out.println("What is your birth year?");
		BirthYear = input.nextInt();
		
		System.out.println("Your birth year multiplied by 2 is: " + (BirthYear * 2));
		// multiplies users birth year by 2
		
		System.out.println("\n");
		
		System.out.println("How tall are you? (in inches)");
		Height = input.nextInt();
		
		System.out.println("Your height in cm is: " + (Height * 2.54) + " Your height in feet is: " + (Height / 12));
		// calculates users height and outputs the value in centimeters and feet
	}

}
