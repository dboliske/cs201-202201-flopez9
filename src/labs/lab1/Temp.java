package labs.lab1;

import java.util.Scanner;

public class Temp {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		double TempF;
		double TempC;
		
		System.out.print("What is the temperature?(in Fahrenheit)");
		TempF = input.nextInt();
		
		System.out.println("The temperature in Celcius is: " + ((TempF - 32) * 5/9));
		//converts given temp to C
		
		System.out.println("\n");
		
		System.out.print("What is the temperature?(in Celsius)");
		TempC = input.nextInt();
		
		System.out.println("The temperature in Fahrenheit is: " + ((TempC * 9/5) + 32));
		//converts given temp to F
	}

}
