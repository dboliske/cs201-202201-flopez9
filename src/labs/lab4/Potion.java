package labs.lab4;

import java.util.Scanner;

public class Potion {
	
	private String name;
	private double strength;
	
	public Potion() {
		
		name = "";
		strength = 0.0;
	}
	
	public Potion(String name, double strength) {
		
		setName(name);
		setStrength(strength);
	}
	
	public void setName(String name) {
		
		this.name = name;
	}
	
	public void setStrength(double strength) {
		
		if (strength >= 0 && strength <= 10) {
			this.strength = strength;
		} else {
			this.strength = 0.0;
		}			
	}
	
	public String getName() {
	
		return name;
	}
	
	public double getStrength() {
		
		return strength;
	}
	
	public String toString() {
		
		return "This potion is a " + name + "potion it boost your stat by: " + strength;
	}
	
	public boolean equals(Potion P) {
		if (name != P.getName()) {
			return false;
		} else if (strength != P.getStrength()) {
			return false;
		}
		return true;
	}
	
	public static Potion constructPotion() {
		
		return constructPotion(new Scanner(System.in));
	}
	
	public static Potion constructPotion(Scanner input) {
		
		Potion P = new Potion();
		
		P.setName(getString(input, "What is the potions name? "));
		P.setStrength(getdouble(input, "How effective is the potion(scale from 1-10)? "));
		
		return P;
	}
	
	private static double getdouble(Scanner input, String prompt) {
		boolean done = false;
		double value = 0.0;
		
		do {
			System.out.println(prompt);
			double line = input.nextInt();
			try {
				value = line;
				done = true;
			} catch (Exception e) {
				System.out.println("'" + line + "'" + "is not acceptable.");
			}
		} while (!done);
		
		return value;
	}

	private static String getString(Scanner input, String prompt) {
		boolean done = false;
		String value = "";
		
		do {
			System.out.println(prompt);
			String line = input.nextLine();
			try {
				value = line;
				done = true;
			} catch (Exception e) {
				System.out.println("'" + line + "'" + "is not acceptable.");
			}
		} while (!done);
		
		return value;
	}
}
