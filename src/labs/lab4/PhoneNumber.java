package labs.lab4;

import java.util.Scanner;

public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		
		countryCode = "";
		areaCode = "";
		number = "";
	}
	
	public PhoneNumber(String countryCode, String areaCode, String number) {
		
		setCountryCode(countryCode);
		setAreaCode(areaCode);
		setNumber(number);
	}
	
	public void setCountryCode(String countryCode) {
		
			this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) {
		
		if (areaCode.length() == 3) {
			this.areaCode =areaCode;
		} else {
			this.areaCode = "";
		}
	}
	
	public void setNumber(String number) {
		
		if (number.length() == 7) {
			this.number = number;
		} else {
			this.number = "";
		}
	}
	
	public String getCountryCode() {
		
		return countryCode;
	}
	
	public String getAreaCode() {
		
		return areaCode;
	}
	
	public String getNumber() {
		
		return number;
	}
	
	public String toString() {
		
		return "+" + countryCode + " (" +  areaCode + ")" + number;
	}
	
	public boolean equals(PhoneNumber P) {
		if (countryCode != P.getCountryCode()) {
				return false;
		} else if (areaCode != P.getAreaCode()) {
			return false;
		} else if (number != P.getNumber()) {
			return false;
		}
		
		return true;
	}
	
	public static PhoneNumber constructPhoneNumber() {
		
		return constructPhoneNumber(new Scanner(System.in));
	}
	
	public static PhoneNumber constructPhoneNumber(Scanner input) {
		
		PhoneNumber P = new PhoneNumber();
		
		P.setCountryCode(getString(input, "Enter your Country code: "));
		P.setAreaCode(getString(input, "Enter your Area code: "));
		P.setNumber(getString(input, "Enter your the rest of your number: "));	
		
		return P;
	}

	private static String getString(Scanner input, String prompt) {
		boolean done = false;
		String value = "";
		
		do {
			System.out.println(prompt);
			String line = input.nextLine();
			try {
				value = line;
				done = true;
			} catch (Exception e) {
				System.out.println("'" + line + "' " + "is not acceptable.");
			}
		} while (!done);
		
		return value;
	}

}