package labs.lab5;

public class CTAStation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		
		super();
		name = "";
		location = "";
		wheelchair = false;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		
		setName(name);
		setLocation(location);
		setWheelchair(wheelchair);
		setOpen(open);
	}
	
	public void setName(String name) {
		
		this.name = name;		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		
		if (wheelchair == true) {
			this.wheelchair = wheelchair;
		} else {
			this.wheelchair = false;
		}
	}
	
	public void setOpen(boolean open) {
		
		if (open == true) {
			this.open = open;
		} else {
			this.open = false;
		}
	}
	
	public String getName() {
		
		return name;
	}
	
	public String getLocation() {
		
		return location;
	}
	
	public boolean hasWheelchair() {
		
		return wheelchair;
	}
	
	public boolean isOpen() {
		
		return open;
	}
	
	public String toString() {
		
		return "The station " + name + " is on " + location + ", it is this far from you " + super.toString();
	}
	
	
}
