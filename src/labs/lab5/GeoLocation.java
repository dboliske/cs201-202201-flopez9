package labs.lab5;

import java.util.Scanner;

public class GeoLocation extends CTAStation {
		
		private double lat;
		private double lng;
		
		public GeoLocation() {
			
			lat = 0;
			lng = 0;
		}
		
		public GeoLocation(double lat, double lng) {
			
			setLat(lat);
			setLng(lng);
			
		}
		
		public void setLat(double lat) {
			
			if (lat >= -90 && 90 >= lat) {
				this.lat = lat;
			} else {
				this.lat = 0;
			}
		}
		
		public void setLng(double lng) {
			
			if (lng >= -180 && 180 >= lng) {
				this.lng = lng;
			} else {
				this.lng = 0;
			}
		}
		
		public double getLat() {
			
			return lat;
		}
		
		public double getLng() {
			
			return lng;
		}
		
		public String toString() {
			
			return "(" + lat + "," + lng + ")";
		}
		
		public boolean equals(GeoLocation g) {
			if (lat != g.getLat()) {
					return false;
			} else if (lng != g.getLng()) {
				return false;
			}
			
			return true;
		}
		
		public double calcDistance(GeoLocation g) {
	        return calcDistance(g.getLat(), g.getLng());
	    }


		private double calcDistance(double lat2, double lng2) {
			Math.sqrt(Math.pow(lat - lat2, 2) + Math.pow(lng - lng2, 2));

			return 0;
		}

		public static GeoLocation constructGeoLocation() {
			
			return constructGeolocation(new Scanner(System.in));
		}
		
		private static GeoLocation constructGeolocation(Scanner scanner) {
			return null;
		}

		public static GeoLocation constructGeoLocation(Scanner input) {
			
			GeoLocation l = new GeoLocation();
			
			l.setLat(getDouble(input, "Enter lat: "));
			l.setLng(getDouble(input, "Enter lng: "));
			
			return l;
		}

		private static double getDouble(Scanner input, String prompt) {			
			boolean done = false;
			double value = 0.0;
			
			do {
				System.out.println(prompt);
				String line = input.nextLine();
				try {
					value = Double.parseDouble(line);
					done = true;
				} catch (Exception e) {
					System.out.println("'" + line + "' is not a valid number.");
				}
			} while (!done);
			
			return value;
		}
		
	}