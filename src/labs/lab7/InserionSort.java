package labs.lab7;

public class InserionSort {
	
	public static String[] InsertionSort(String[] array) {
		
		int i;
		int j;
		
		for (j = 1; j < array.length; j++) {
			i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) <=0) {
				swap(array, i, i-1);
				i--;
			}
		}
		return array;
	}
	
	public static void swap(String[] array, int i, int j) {
		
		String temp = array[i];
		array[i] = array[j];
		array[j] = temp;
		
	}
	
	public static void main(String[] args) {
		
		String[] words = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		words = InsertionSort(words);
		
		for (String w : words) {
			System.out.println(w + " ");
		}

	}

}
