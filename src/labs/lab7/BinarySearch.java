package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	public static int BinarySearch(String[] array, String val) {
		return BinarySearch(array, val, 0, array.length);
	}
	
	public static int BinarySearch(String[] array, String val, int start, int end) {
		if (start >= end) {
			return -1;
		}
		
		int middle = (start + end) / 2;
		if (array[middle].compareToIgnoreCase(val) < 0) {
			return BinarySearch(array, val, middle + 1, end);
		} else if (array[middle].compareToIgnoreCase(val) > 0) {
			return BinarySearch(array, val, start, middle);
		}
		return middle;
	}
	
	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String val = input.nextLine();
		int index = BinarySearch(lang, val);
		if (index == -1) {
			System.out.println(val + " not found.");
		} else {
			System.out.println(val + " found at " + index + ".");
		}
		
		input.close();
	}
}
