package labs.lab7;

public class BubbleSort {

		public static int[] bubbleSort(int[]list) {
			
			int i;
			int j;
			int temp = 0;
			
			for (i = 0; i < list.length - 1; i++) {
				for (j = 0; j < list.length - 1 - i; j++) {
					if (list[j] > list[j + 1]) {
						temp = list[j];
						list[j] = list[j + 1];
						list[j + 1] = temp;
					}
				}
			}
			return list;		
		}
		
		public static void main(String[]args) {
			int numbers[] = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
			
			numbers = bubbleSort(numbers);
			
			for (int n : numbers) {
				System.out.println(n + " ");
			}
		}

}
