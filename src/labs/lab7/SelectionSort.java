package labs.lab7;

public class SelectionSort {
	
	public static double[] selectionsort(double[] a) {
		int start;
		double temp;
		
		for (int i = a.length - 1; i > 0; i--) {
			start = 0;
			
			for (int j = 1; j <= 1; j++) {
				if (a[j] >  a[start]) {
					start = j;
				}
			}
			
			temp = a[start];
			a[start] = a[i];
			a[i] = temp;			
		}
		return a;			
	}
	
	public static void main(String[] args) {
		
		double[] a = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		for (double i : selectionsort(a)) {
			System.out.println(i);
		}
	}

}
