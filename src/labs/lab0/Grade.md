# Lab 0

## Total

18/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       4/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          1/2
  * Correct output matches pseudocode   1/2
* Documentation                         2/2

## Comments
