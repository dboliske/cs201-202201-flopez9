package project;

public class ProduceItem extends Item {
	
	private String date;	

	public ProduceItem() {
		
		super();
		date = "";		
	}
	
	public ProduceItem(String name, double price, int inventory, String date) {
		
		super(name, price, inventory);
		
		setDate(date);
	}
	
	public void setDate(String date) {
		
		this.date = date;
	}
	
	public String getDate() {
		
		return date;
	}
	
	public String toString() {
		
		return "The item " + getName() + " is a Produce Item it will expire on " + date;
	}
	
	public boolean equals(ProduceItem p) {
		
		if (date != p.getDate()) {
			return false;
		}
		
		return true;
	}
}
