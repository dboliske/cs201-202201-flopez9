package project;

public class Item {
	
	private String name;
	private double price;
	private int inventory;
	
	public Item() {
		
		name = "";
		price = 0.0;
		inventory = 0;
	}
	
	public Item(String name, double price, int inventory) {
		
		setName(name);
		setPrice(price);
		setInventory(inventory);
	}
	
	public void setName(String name) {
		
		this.name = name;
	}
	
	public void setPrice(double price) {
		
		if (price >= 0.0) {
			this.price = price;
		} else if (price < 0.0) {
			this.price = 0.0;
		}
	}
	
	public void setInventory(int inventory) {
		
		if (inventory >=  0) {
			this.inventory = inventory;
		} else if (inventory < 0) {
			this.inventory = 0;
		}
	}

	public String getName() {
		
		return name;
	}
	
	public double getPrice() {
		
		return price;
	}
	
	public int getInventory() {
		
		return inventory;
	}
	
	public String toString() {
		
		return "The item " + name + " costs " + price + ", there is " + inventory + " of this item in stock.";
	}
	
	public boolean equals(Item i) {
		
		if (name != i.getName()) {
			return false;
		} else if (price != i.getPrice()) {
			return false;
		} else if (inventory != i.getInventory()) {
			return false;
		}
		
		return true;
	}
}
