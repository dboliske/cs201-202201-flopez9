package project;

public class ShelvedItem extends Item{
	
	private String date;	

	public ShelvedItem() {
		
		super();
		date = "";		
	}
	
	public ShelvedItem(String name, double price, int inventory, String date) {
		
		super(name, price, inventory);
		
		setDate(date);
	}
	
	public void setDate(String date) {
		
		this.date = date;
	}
	
	public String getDate() {
		
		return date;
	}
	
	public String toString() {
		
		return "The item " + getName() + " is a Shelved Item it will expire on " + date;
	}
	
	public boolean equals(ShelvedItem s) {
		
		if (date != s.getDate()) {
			return false;
		}
		
		return true;
	}
}

