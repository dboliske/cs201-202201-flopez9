package project;

public class AgeRestricted extends Item {

	private int age;
	
	public AgeRestricted() {
		
		super();
		age = 0;
	}
	
	public AgeRestricted(String name, double price, int inventory, int age) {
		
		super(name, price, inventory);
		
		setAge(age);
	}
	
	public void setAge(int age) {
		
		this.age =age;
	}
	
	public int getAge() {
		
		return age;
	}
	
	public String toString() {
		
		return "The item " + getName() + " is an Age Restricted Item you must be " + age + " to purchase this item";
	}
	
	public boolean equals(AgeRestricted a) {
		
		if  (age != a.getAge()) {
			return false;
		}
		
		return true;
	}
	
}
